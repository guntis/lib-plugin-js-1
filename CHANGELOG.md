## [6.5.6] 2019-07-03
## Fixed
- Wrong ad join duration for first ad for non first views.

## [6.5.5] 2019-06-19
## Fixed
- Forced code for view and session stop events.
- Forced context for session event.
- Simplified `init` logic and chronos.
- P2P not being reported
## Removed
- `Sessions` from `beat` requests
## Added
- `device.isAnonymous` option

## [6.5.4] 2019-06-17
## Added
- `ad.provider` option, `getProvider` method for adapter `provider` parameter in `adStart`
- Fingerprint now is deviceUUID, simplified logic.
- Device metadata now sent in `deviceInfo`.
## Removed
- `preload` metric
## Fixed
- Chrono for jointime with prerolls when adapter `init` is called manually
- Protected `code` parameter to fireError() overrides

## [6.5.3] 2019-06-06
## Removed
- `Sessions` parameter in `beat`
## Fixed
- Https for adblock detector

## [6.5.2] 2019-05-31
## Fixed
- Postrolls after stop fix

## [6.5.1] 2019-05-31
## Removed
- ParentId for sessions
## Fixed
- Jointime calculation when the view has init

## [6.5.0] 2019-05-30
## Added
- Smartads v3.0
- Removed standardadapter
- Some options deprecated, a warning is shown if the used option has a new name
## Important notes
- Adapters with version 6.4.x and lower may not be compatible with this lib.
If there is no 6.5.0 or higher version for the adapter you need please ask plugins@nicepeopleatwork.com

## [6.4.30] 2019-05-24
## Added
- `parse.fdsResponseHost` option

## [6.4.29] 2019-05-22
## Fixed
- Wrong jointime for non-first views, with prerolls

## [6.4.28] 2019-05-13
## Changed
- Akamai parser

## [6.4.27] 2019-05-09
## Changed
- Default host from `nqs.nice264.com` to `a-fds.youborafds01.com`

## [6.4.26] 2019-04-30
## Fixed
- Parse hls feature when the manifest has more than 2 levels in the same host.

## [6.4.25] 2019-04-18
## Added
- `parse.dashLocation`feature, parsing dash manifests with `location` tag.

## [6.4.24] 2019-04-12
## Added
- Extra check to prevent wrong ad positions
## Fixed
- Minor fixes, found improving test coverage

## [6.4.23] 2019-04-05
## Added
- Feature to delay start event, using `waitForMetadata` and `pendingMetadata` options.

## [6.4.22] 2019-03-28
## Added
- `fireEvent` method for adapters to report custom events.
- `content.metrics` and `session.metrics`options to report custom metrics in ping and beat events.
- `getMetrics` method in adapter, to report custom metrics in ping.
- Automatic data request every hour if no views or sessions are active.
- New options: `user.email`, `content.package`, `content.saga`, `content.tvShow`, `content.season`, `content.episodeTitle`, `content.channel`, `content.id`, `content.imdbId`, `content.gracenoteId`, `content.type`, `content.genre`, `content.language`, `content.subtitles`, `content.contractedResolution`, `content.cost`, `content.price`, `content.playbackType`, `content.drm`, `content.encoding.videoCodec`, `content.encoding.audioCodec`, `content.encoding.codecSettings`, `content.enconding.codecProfile`, `content.encoding.containerFormat`,
- New option alias: `user.type`, `user.name`, `user.obfuscateIp`, `user.anonymousId`, `app.https`, `background.settings.iOS`, `content.customdimension.x`instead of `customDimension.x`
- Deprecated `isInfinity`
### Fixed
- Minor infinity fixes

## [6.4.21] 2019-03-26
## Fixed
- Wrong jointime for views without init with prerolls.

## [6.4.20] 2019-03-13
## Added
- Retry for requests with response code higher than 399

## [6.4.19] 2019-03-06
## Added
- `preventZombieViews` option, true by default

## [6.4.18] 2019-03-05
## Added
- `npawPluginUUID` parameter
## Fixed
- Adapter not working when it needs no player reference

## [6.4.17] 2019-02-21
## Fixed
- `deviceCode` not working properly

## [6.4.16] 2019-01-30
## Added
 - Telefonica CDN Host and type detection
 - `app.name` and `app.releaseVersion` options
 - Check to prevent wrong values as ad position
## Fixed
 - CDN Host and type not being reported always
 - Device options being not sent

## [6.4.15] 2019-01-30
## Fixed
 - AdAdapter closing views when ad ended

## [6.4.14] 2019-01-29
## Fixed
 - Stop not being sent when called by the adapter, after a manual call of init
## Added
 - Closing buffers when pause begins
 - Updated fingerprint
 - Alias for `extraparam` : `customDimension`
 - Alias for `title2`: `program`
 - Device options: `device.brand`, `device.type`, `device.model`, `device.name`, `device.osName`,
 `device.osVersion`,`device.browserName`,`device.browserVersion`, `device.browserType`, `device.browserEngine`
 - setExtraParams methods: `setCustomDimensions` and `setAdCustomDimensions`
 - Infinity internal refactor
 - Cookies disabled by default, no fallback.
 - Libversion reported in start events
 - Teltoo p2p support

## [6.4.13] 2018-12-27
## Added
 - Automatic polyfill when needed (`bind` and `foreach`)
 - System sent in all events
 - Infinity events not reported without sessionId

## [6.4.12] 2018-12-18
## Fixed
 - Wrong jointime calculation with multiple prerolls and init

## [6.4.11] 2018-11-23
## Added
 - `newSession` method for infinity
## Fixed
 - `InfinityStarted` using wrong storage

## [6.4.10] 2018-11-21
## Fixed
 - Nav after a navigation when data is requested in a previous page but start is not sent
 - NaN renditions, renditions with non numeric value as a bitrate

## [6.4.9] 2018-10-18
## Added
 - Smartswitch options: `smartswitch.configCode`, `smartswitch.groupCode` and `smartswitch.contractCode`
 - Teltoo p2p

## [6.4.8] 2018-09-17
## Fixed
 - ParseHls resource priority over content.resource option

## [6.4.7] 2018-09-04
## Added
 - `forceInit` option

## [6.4.6] 2018-08-30
## Fixed
 - Forced to send session root for infinity events

## [6.4.5] 2018-08-29
## Fixed
 - Sessions not reported properly on few devices
 - Pause duration in stop event, after a view with pause and resume

## [6.4.4] 2018-08-28
## Added
 - Check for infinity between pages not sharing localstorage

## [6.4.3] 2018-08-28
## Added
 - device code for infinity

## [6.4.2] 2018-08-27
## Fixed
 - host race condition for infinity

## [6.4.1] 2018-08-16
## Added
 - isInfinity option

## [6.4.0] 2018-08-16
## Added
 - New infinity design: methods rework
 - Infinity register values
 - Fingerprint
 - Use of cookies when no storages available
 - Referral and language report
 - AnonymousUser option
 - Background detector for infinity
 - Viewcodes with timestamp instead of numbers
 ## Removed
 - Unused values from entities
 - `session.expire`option
 ## Fixed
 - Now stop can be called after init (without start)

## [6.3.5] 2018-08-02
## Fixed
 - LocationHeader resource priority over content.resource option

## [6.3.4] 2018-07-17
## Fixed
 - Iterating Streamroot instances as array

## [6.3.3] 2018-07-11
## Fixed
 - Streamroot not working

## [6.3.2] 2018-07-06
## Fixed
 - Location header functionality
 - Blocked error event flood
 - Sending adErrors after session started

## [6.3.1] 2018-07-04
## Added
 - Support for new streamroot api
## Removed
 - Wrong throughput value calculated with p2p plugins
## Fixed
 - Ad number increase during adErrors.

## [6.3.0] 2018-06-14
## Added
 - Username reported in /data when available
 - New adapter object to extend (StandardAdapter) making easier to implement adapters for html5ish players.
 - Offline mode
 - `Offline` option
 - Fatal error not sending 'fatal' level
 - Fireclick can be used with string parameter as url directly
 - fireSkip() method (fireStop({skipped: true}) alias)
## Fixed
 - Jointime value for views started with ads and without adapter

## [6.2.6] 2018-05-15
## Fixed
 - Aderror cant create views
 - Jointime value when used plugin.fireInit() instead of plugin.getAdapter().fireInit()/start

## [6.2.5] 2018-05-10
## Fixed
 - Now an adstart can start a view (sending an init/start before)
 - Stop being reported twice when called from the plugin after being called from the adapter.
 - Fixed ad error report creating new views and being reported as errors

## [6.2.4] 2018-05-08
## Fixed
 - Forced start/init before adError to track preroll/precontent ad errors
## Added
 - String constants for ad positions `Adapter.AdPosition` PRE, MID and POST

## [6.2.3] 2018-04-27
## Added
 - getLatency, getPacketLoss, getPacketSent adapter methods
 - reporting latency, packetLoss and packetSent in pings
 - `content.isLive.noSeek` option to ignore seeks, only for live content.
## Fixed
 - Wrong value for resource was sent when it was set as an option after init and before start

## [6.2.2] 2018-04-13
## Removed
 - Duration and playhead for live content

## [6.2.1] 2018-04-12
## Added
 - SmartTV device type

## [6.2.0] 2018-04-06
## Added
 - Device (android, iphone and desktop) detection
 - Background detection
 - Background.settings, background.enabled, background.settings for each device type options.
## Fixed
 - Reset pause seek and buffer chronos after stop is sent
 - Removed errorLevel value by default

## [6.1.15] 2018-03-28
## Fixed
 - Restart isStarted plugin flag

## [6.1.14] 2018-03-21
## Added
 - Experiment option
 - Referer option, to set it manually

## [6.1.13] - 2018-03-02
## Fixed
 - Fixed view code when view starts with an error
 - Fixed double start sent in some cases
 - Fixed wrong jointime when using plugin init
 - Viewcode changed for frozen views
 ## Added
 - Cancel buffer method
 - Cancel seek method
 - getHouseshold method in adapter
 - parse.locationHeader option

## [6.1.12] - 2018-02-13
## Fixed
 - Fixed pauseDuration not being sent in stop
 - Adapter fireInit behaviour with auto start
 - Prevent crash if storage is not available
## Added
 - cdnType and cdnNode can be set as an option
 - fireCasted method for adapter

## [6.1.11] - 2018-01-30
## Added
 - Buffer monitor for custom playrates

## [6.1.10] - 2018-01-29
## Added
 - IsP2PEnabled for peer5
 - Added 10 ad extraparam
 - Reported playhead for live content

## [6.1.9] - 2018-01-16
## Added
 - Added userType and content.streamingProtocol options
 ## Fixed
 - Changed p2p params names

## [6.1.8] - 2017-12-22
## Fixed
 - Fixed access to storages when is not possible but exists
 - Added ad.campaign, ad.title and ad.resource options
 - Removed background detection

## [6.2.0-beta2] - 2017-12-18
## Added
 - Request number param to prevent /data calls being cached
 - P2PEnabled param activated

## [6.2.0-beta] - 2017-12-05
## Added
 - Device detection (android, iphone, desktop)
 - Background detection and actions
 - Options for background detection 'autoDetectBackground' and 'autoDetectBackgroundSettings'

## [6.1.7] - 2017-12-04
## Fixed
 - Now error message is 'msg' instead of 'errorMsg'

## [6.1.6] - 2017-11-28
## Fixed
 - FatalError now sends error and stop
 - Stop event sends pauseDuration if needed

## [6.1.5] - 2017-11-23
## Added
 - P2P enabled parameter getter
 - Option obfuscateIP

## [6.1.4] - 2017-11-14
### Fixed
 - Semicolons no longer ignored in hlsparser
 - IsLive detection when start
 - resource.js init fix
 - Preventing to send init twice
## Added
 - Automatic use of adInit when needed

## [6.1.3] - 2017-10-31
### Fixed
 - Several issues with init, start and jointime events

## [6.1.0] - 2017-10-23
### Added
- Support for Streamroot and peer5 p2p metrics
- Send player name on start
- Send media duration and resource on jointime
- Get throughput values by default when using p2p
- Remove playhead and media duration of live content

## [6.0.10] - 2017-10-16
### Added
- FireStop with end param to end views with ad.afterStop option

## [6.0.9] - 2017-10-10
### Added
- Option ad.afterStop to catch ads after player stop

## [6.0.8] - 2017-10-04
### Added
- Option ad.ignore to block AdsAdapter messages

## [6.0.7] - 2017-10-02
### Added
- Add playhead on ad events

## [6.0.6] - 2017-09-21
### Added
- Add p2p metrics

## [6.0.5] - 2017-07-24
### Added
- Add init and adInit events, called with `fireInit`
- Add `fireNoServedAd` as special error

## [6.0.4] - 2017-07-24
### Added
- add `isDone` property to `ResourceTransform`.
- Add more `extraParam`, up to 20.

## [6.0.3] - 2017-06-06
### Changed
- Removed `manifest.json` from repo.
- Removed `manifest.json` from `.npmignore`.

## [6.0.2] - 2017-06-06
### Added
- `username` added to infinity's `nav`.

### Git
- Added `manifest.json` to `.gitignore`.

## [6.0.1] - 2017-06-01
### Fixed
- Fix several `infinity` module issues.

### Added
- Support for `adError`, `adClick` and `adBlocked`.
- Add `getComm` to `Plugin` and `Infinity`.
- Add `npm run clean` command to delete `dist`, `coverage` and `deploy` folders.

## [6.0.0] - 2017-05-30
### Fixed
- Fixed CDN Parser issues
- Fixed /error before /init

## [6.0.0-rc.infinity] - 2017-05-19
### Added
- Infinity module

## [6.0.0-rc] - 2017-01-09
### Added
- First release

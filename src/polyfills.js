/* eslint no-extend-native: "off" */

/**
 * When executed, this function applies polyfills to the following functionalities:
 * Function.prototype.bind and
 * Array.prototype.forEach.
 *
 * @memberof youbora
 */
var applyPolyfills = function () {
  // Bind
  Function.prototype.bind = Function.prototype.bind || function (b) {
    if (typeof this !== 'function') {
      throw new TypeError('Function.prototype.bind - what is trying to be bound is not callable')
    }
    var a = Array.prototype.slice
    var f = a.call(arguments, 1)
    var e = this
    var C = function () { }
    var d = function () {
      return e.apply(this instanceof C ? this : b || window, f.concat(a.call(arguments)))
    }
    C.prototype = this.prototype
    d.prototype = new C()
    return d
  }

  // Foreach
  Array.prototype.forEach = Array.prototype.forEach || function (callback, thisArg) {
    if (typeof (callback) !== 'function') {
      throw new TypeError(callback + ' is not a function!')
    }
    var len = this.length
    for (var i = 0; i < len; i++) {
      callback.call(thisArg, this[i], i, this)
    }
  }
}

module.exports = applyPolyfills

var YouboraObject = require('../object')

var DataExtractor = YouboraObject.extend({
  constructor: function (plugin) {
    this.plugin = plugin
  },

  getAllData: function () {
    var returnValue = this.getNonRandomData()
    returnValue.timestamp = this.getTimestamp()
    return returnValue
  },

  getNonRandomData: function () {
    var returnValue = {}
    returnValue.userAgent = this.getUserAgent()
    returnValue.threads = this.getVirtualCores()
    returnValue.language = this.getLanguage()
    returnValue.langList = this.getAvailableLanguages()
    returnValue.resolution = this.getResolution()
    returnValue.colorDepth = this.getColorDepth()
    returnValue.deviceMemory = this.getMemory()
    returnValue.touchscreen = this.getTouchscreen()
    returnValue.localStorage = this.getLocalStorage()
    returnValue.sessionStorage = this.getSessionStorage()
    returnValue.cookiesAvailable = this.getCookiesAvailable()
    returnValue.flashAvailable = this.getHasFlash()
    returnValue.timeZone = this.getTimeZone()
    returnValue.plugins = this.getPluginList()
    return returnValue
  },

  // Getters
  getUserAgent: function () {
    return this._navigatorCheck() ? navigator.userAgent : null
  },

  getVirtualCores: function () {
    return this._navigatorCheck() ? navigator.hardwareConcurrency : null
  },

  getLanguage: function () {
    return this._navigatorCheck() ? navigator.language : null
  },

  getAvailableLanguages: function () {
    return this._navigatorCheck() ? navigator.languages : null
  },

  getResolution: function () {
    if (this._navigatorCheck() && navigator.screen) {
      return navigator.screen.width.toString() + navigator.screen.height.toString()
    }
    return null
  },

  getColorDepth: function () {
    return (this._navigatorCheck() && navigator.screen) ? navigator.screen.colorDepth : null
  },

  getMemory: function () {
    return this._navigatorCheck() ? navigator.deviceMemory : null
  },

  getTouchscreen: function () {
    return this._navigatorCheck() ? navigator.maxTouchPoints : null
  },

  getLocalStorage: function () {
    return typeof localStorage !== 'undefined'
  },

  getSessionStorage: function () {
    return typeof sessionStorage !== 'undefined'
  },

  getCookiesAvailable: function () {
    return this._navigatorCheck() ? navigator.cookieEnabled : false
  },

  getHasFlash: function () {
    if (!this._navigatorCheck()) return false
    return (typeof navigator.plugins !== 'undefined' && typeof navigator.plugins['Shockwave Flash'] === 'object') || (window.ActiveXObject && (new ActiveXObject('ShockwaveFlash.ShockwaveFlash')) != false)
  },

  getPluginList: function () {
    if (!this._navigatorCheck() || !navigator.plugins || navigator.plugins.length === 0) return null
    var pluginlist = ''
    for (var counter = 0; counter < navigator.plugins.length; counter++) {
      pluginlist += navigator.plugins[counter].description + ' ' + navigator.plugins[counter].filename + ' ' + navigator.plugins[counter].name + ' '
    }
    return pluginlist
  },

  getTimeZone: function () {
    var date = new Date()
    return date.getTimezoneOffset().toString()
  },

  getTimestamp: function () {
    return new Date().getTime()
  },

  _navigatorCheck: function () {
    return typeof navigator !== 'undefined'
  }

})

module.exports = DataExtractor

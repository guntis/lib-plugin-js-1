var Log = require('../log')
var Constants = require('../constants')

var Adapter = require('../adapter/adapter')

// This file is designed to add extra functionalities to Plugin class

/** @lends youbora.Plugin.prototype */
var PluginContentMixin = {
  /**
   * Sets an adapter for video content.
   *
   * @param {Adapter} adapter
   *
   * @memberof youbora.Plugin.prototype
   */
  setAdapter: function (adapter) {
    if (adapter.plugin) {
      Log.warn('Adapters can only be added to a single plugin')
    } else {
      this.removeAdapter()

      this._adapter = adapter
      adapter.plugin = this

      // Register listeners
      adapter.on(Adapter.Event.START, this._startListener.bind(this))
      adapter.on(Adapter.Event.JOIN, this._joinListener.bind(this))
      adapter.on(Adapter.Event.PAUSE, this._pauseListener.bind(this))
      adapter.on(Adapter.Event.RESUME, this._resumeListener.bind(this))
      adapter.on(Adapter.Event.SEEK_BEGIN, this._seekBeginListener.bind(this))
      adapter.on(Adapter.Event.SEEK_END, this._seekEndListener.bind(this))
      adapter.on(Adapter.Event.BUFFER_BEGIN, this._bufferBeginListener.bind(this))
      adapter.on(Adapter.Event.BUFFER_END, this._bufferEndListener.bind(this))
      adapter.on(Adapter.Event.ERROR, this._errorListener.bind(this))
      adapter.on(Adapter.Event.STOP, this._stopListener.bind(this))
      adapter.on(Adapter.Event.VIDEO_EVENT, this._videoEventListener.bind(this))
    }
  },

  /**
   * Returns current adapter or null.
   *
   * @returns {Adapter}
   *
   * @memberof youbora.Plugin.prototype
   */
  getAdapter: function () {
    return this._adapter
  },

  /**
   * Removes the current adapter. Fires stop if needed. Calls adapter.dispose().
   *
   * @memberof youbora.Plugin.prototype
   * */
  removeAdapter: function () {
    if (this._adapter) {
      this._adapter.dispose()
      this._adapter.plugin = null

      this._adapter.off(Adapter.Event.START, this._startListener)
      this._adapter.off(Adapter.Event.JOIN, this._joinListener)
      this._adapter.off(Adapter.Event.PAUSE, this._pauseListener)
      this._adapter.off(Adapter.Event.RESUME, this._resumeListener)
      this._adapter.off(Adapter.Event.SEEK_BEGIN, this._seekBeginListener)
      this._adapter.off(Adapter.Event.SEEK_END, this._seekEndListener)
      this._adapter.off(Adapter.Event.BUFFER_BEGIN, this._bufferBeginListener)
      this._adapter.off(Adapter.Event.BUFFER_END, this._bufferEndListener)
      this._adapter.off(Adapter.Event.ERROR, this._errorListener)
      this._adapter.off(Adapter.Event.STOP, this._stopListener)
      this._adapter.off(Adapter.Event.VIDEO_EVENT, this._videoEventListener)

      this._adapter = null
    }
  },

  // ---------------------------------------- LISTENERS -----------------------------------------
  _startListener: function (e) {
    if (!this.isInitiated) {
      this.viewTransform.nextView()
      this._initComm()
      this._startPings()
    } else if (this.initChrono.startTime !== 0) {
      this._adapter.chronos.join.startTime = this.initChrono.startTime
    }
    var params = e.data.params || {}
    var allParamsReady = this.getResource() && typeof this.getIsLive() === 'boolean' &&
      (this.getIsLive() || (typeof this.getDuration() === 'number' && this.getDuration() > 0)) && this.getTitle()
    allParamsReady = this.options['forceInit'] ? false : (allParamsReady && this._isExtraMetadataReady())
    if (allParamsReady && !this.isInitiated) { // start
      this._send(Constants.WillSendEvent.WILL_SEND_START, Constants.Service.START, params)
      this._adSavedError()
      this._adSavedManifest()
      Log.notice(Constants.Service.START + ' ' + (params.title || params.mediaResource))
      this.isStarted = true
      // chrono if had no adapter when inited
    } else if (!this.isInitiated) { // init
      this.isInitiated = true
      this._adapter.chronos.join.start()
      this._send(Constants.WillSendEvent.WILL_SEND_INIT, Constants.Service.INIT, params)
      this._adSavedError()
      this._adSavedManifest()
      Log.notice(Constants.Service.INIT + ' ' + (params.title || params.mediaResource))
    }
  },

  _retryStart: function (e) {
    if (this._isExtraMetadataReady()) {
      this._send(Constants.WillSendEvent.WILL_SEND_START, Constants.Service.START, {})
      this.startDelayed = false
    }
  },

  _joinListener: function (e) {
    var params = e.data.params || {}
    if (!this._adsAdapter || !this._adsAdapter.flags.isStarted) {
      if (this._adsAdapter) {
        this._adapter.chronos.join.startTime = Math.min(
          this._adapter.chronos.join.startTime + (this._totalPrerollsTime || 0),
          new Date().getTime()
        )
        this._totalPrerollsTime = 0
      }
      if (this.isInitiated && !this.isStarted) { // start if just inited
        if (this._isExtraMetadataReady()) {
          this._send(Constants.WillSendEvent.WILL_SEND_START, Constants.Service.START, params)
        } else {
          this.startDelayed = true
        }
        this._adSavedError()
        this._adSavedManifest()
        Log.notice(Constants.Service.START + ' ' + (params.title || params.mediaResource))
        this.isStarted = true
      }
      params = e.data.params || {}
      if (this._adsAdapter && this.isBreakStarted) {
        this._adsAdapter.fireBreakStop()
      }
      this._send(Constants.WillSendEvent.WILL_SEND_JOIN, Constants.Service.JOIN, params)
      Log.notice(Constants.Service.JOIN + ' ' + params.joinDuration + 'ms')
    } else { // If it is currently showing ads, join is invalidated
      if (this._adapter.monitor) this._adapter.monitor.stop()
      this._adapter.flags.isJoined = false
      this._adapter.chronos.join.stopTime = 0
    }
  },

  _pauseListener: function (e) {
    if (this._adapter) {
      if (this._adapter.flags.isBuffering ||
        this._adapter.flags.isSeeking ||
        (this._adsAdapter && this._adsAdapter.flags.isStarted)) {
        this._adapter.chronos.pause.reset()
      }
    }

    var params = e.data.params || {}
    this._send(Constants.WillSendEvent.WILL_SEND_PAUSE, Constants.Service.PAUSE, params)
    Log.notice(Constants.Service.PAUSE + ' at ' + params.playhead + 's')
  },

  _resumeListener: function (e) {
    if (this._adsAdapter && this.isBreakStarted && !this._adsAdapter.flags.isStarted) {
      this._adsAdapter.fireBreakStop()
    }
    var params = e.data.params || {}
    this._send(Constants.WillSendEvent.WILL_SEND_RESUME, Constants.Service.RESUME, params)
    Log.notice(Constants.Service.RESUME + ' ' + params.pauseDuration + 'ms')
    this._adapter.chronos.pause.reset()
  },

  _seekBeginListener: function (e) {
    if (this._adapter && this._adapter.flags.isPaused) this._adapter.chronos.pause.reset()
    Log.notice('Seek Begin')
  },

  _seekEndListener: function (e) {
    var params = e.data.params || {}
    this._send(Constants.WillSendEvent.WILL_SEND_SEEK, Constants.Service.SEEK, params)
    Log.notice(Constants.Service.SEEK +
      ' to ' + params.playhead +
      ' in ' + params.seekDuration + 'ms'
    )
  },

  _bufferBeginListener: function (e) {
    if (this._adapter && this._adapter.flags.isPaused) this._adapter.chronos.pause.reset()
    Log.notice('Buffer Begin')
  },

  _bufferEndListener: function (e) {
    var params = e.data.params || {}
    this._send(Constants.WillSendEvent.WILL_SEND_BUFFER, Constants.Service.BUFFER, params)
    Log.notice(Constants.Service.BUFFER +
      ' to ' + params.playhead +
      ' in ' + params.bufferDuration + 'ms'
    )
  },

  _errorListener: function (e) {
    if (!this._blockError(e.data.params)) {
      this.fireError(e.data.params || {})
      this._adSavedError()
      this._adSavedManifest()
    }
  },

  _blockError: function (errorParams) {
    var now = Date.now()
    var sameError = this._lastErrorParams
      ? this._lastErrorParams.errorCode === errorParams.errorCode && this._lastErrorParams.msg === errorParams.msg : false
    if (sameError && this._lastErrorTime + 5000 > now) {
      this._lastErrorTime = now
      return true
    }
    this._lastErrorTime = now
    this._lastErrorParams = errorParams
    return false
  },

  _stopListener: function (e) {
    this.fireStop(e.data.params || {})
  },

  _isStopReady: function (e) {
    if (this.options['ad.expectedPattern'] && this.options['ad.expectedPattern'].post &&
      this.options['ad.expectedPattern'].post[0] > this.playedPostrolls) {
      return false
    }
    return true
  },

  _videoEventListener: function (e) {
    this._send(Constants.WillSendEvent.VIDEO_EVENT, Constants.Service.VIDEO_EVENT, e.data.params)
  },

  _isExtraMetadataReady: function (e) {
    // If the option is disabled, always is ready
    if (!this.options['waitForMetadata'] || this.options['pendingMetadata'].length < 1) return true
    // If for existing parameters, one of them is false (no value for it), return false
    var getters = this.requestBuilder.getGetters()
    return this.options['pendingMetadata'].map(function (element) {
      if (getters.hasOwnProperty(element)) {
        return !!this[getters[element]]()
      }
    }.bind(this)).indexOf(false) < 0
  }
}

module.exports = PluginContentMixin

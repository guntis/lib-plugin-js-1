var Transform = require('./transform')

var OfflineTransform = Transform.extend(
  /** @lends youbora.OfflineTransform.prototype */
  {
    /**
     * This class manages Fastdata service and view index.
     *
     * @constructs
     * @extends youbora.Transform
     * @memberof youbora
     *
     * @param {Plugin} plugin Instance of {@link Plugin}
     * @param {string} session If provided, plugin will use this as a FD response.
     */
    constructor: function (plugin, session) {
      this._sendRequest = false
      this._isBusy = false
      this.plugin = plugin
      this.session = session
      this.transformName = 'Offline'
    },

    /**
     * Transform requests
     * @param {youbora.comm.YBRequest} request YBRequest to transform.
     */
    parse: function (request) {
      if (request) {
        this.plugin.offlineStorage.addEvent(request.service, request.params)
      }
    },

    hasToSend: function (request) {
      return false
    },

    getState: function () {
      return Transform.STATE_OFFLINE
    }

  })

module.exports = OfflineTransform

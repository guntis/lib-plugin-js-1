describe('ChronoStatus', () => {
  var PlaybackChronos = require('../../../src/adapter/playbackchronos')

  it('should reset propperly', () => {
    var cs = new PlaybackChronos()
    cs.join.start()
    cs.seek.start()
    cs.pause.start()
    cs.buffer.start()
    cs.total.start()

    expect(cs.join.startTime).toBeGreaterThan(1)
    expect(cs.seek.startTime).toBeGreaterThan(1)
    expect(cs.pause.startTime).toBeGreaterThan(1)
    expect(cs.buffer.startTime).toBeGreaterThan(1)
    expect(cs.total.startTime).toBeGreaterThan(1)

    cs.reset()

    expect(cs.join.startTime).toBe(0)
    expect(cs.seek.startTime).toBe(0)
    expect(cs.pause.startTime).toBe(0)
    expect(cs.buffer.startTime).toBe(0)
    expect(cs.total.startTime).toBe(0)
  })
})

describe('FlagStatus', () => {
  var PlaybackFlags = require('../../../src/adapter/playbackflags.js')

  it('should reset propperly', () => {
    var fs = new PlaybackFlags()
    fs.isStarted = true
    fs.isJoined = true
    fs.isPaused = true
    fs.isSeeking = true
    fs.isBuffering = true

    expect(fs.isStarted).toBe(true)
    expect(fs.isJoined).toBe(true)
    expect(fs.isPaused).toBe(true)
    expect(fs.isSeeking).toBe(true)
    expect(fs.isBuffering).toBe(true)

    fs.reset()

    expect(fs.isStarted).toBe(false)
    expect(fs.isJoined).toBe(false)
    expect(fs.isPaused).toBe(false)
    expect(fs.isSeeking).toBe(false)
    expect(fs.isBuffering).toBe(false)
  })
})

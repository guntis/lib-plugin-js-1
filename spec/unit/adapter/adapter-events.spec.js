describe('GenericAdapter Events', () => {
  var Adapter = require('../../../src/adapter/adapter')

  it('should /init', () => {
    var adapter = new Adapter(true)
    adapter.plugin = {
      fireInit: function () { }
    }
    spyOn(adapter.plugin, 'fireInit')
    adapter.fireInit()
    expect(adapter.plugin.fireInit).toHaveBeenCalled()
  })

  it('should /start', (done) => {
    var adapter = new Adapter(true)
    adapter.on(Adapter.Event.START, (e) => {
      expect(e.type).toBe(Adapter.Event.START)
      done()
    })
    adapter.fireStart()

    expect(adapter.flags.isStarted).toBe(true)
    expect(adapter.chronos.join.startTime).not.toBe(0)
    expect(adapter.chronos.total.startTime).not.toBe(0)
  })

  it('should /join', (done) => {
    var adapter = new Adapter(true)
    adapter.on(Adapter.Event.JOIN, (e) => {
      expect(e.type).toBe(Adapter.Event.JOIN)
      done()
    })
    adapter.fireStart()
    adapter.fireJoin()

    expect(adapter.flags.isJoined).toBe(true)
    expect(adapter.chronos.join.stopTime).not.toBe(0)
  })

  it('should /pause', (done) => {
    var adapter = new Adapter(true)
    adapter.on(Adapter.Event.PAUSE, (e) => {
      expect(e.type).toBe(Adapter.Event.PAUSE)
      done()
    })

    adapter.fireStart()
    adapter.fireJoin()
    adapter.firePause()

    expect(adapter.flags.isPaused).toBe(true)
    expect(adapter.chronos.pause.startTime).not.toBe(0)
  })

  it('should /resume', (done) => {
    var adapter = new Adapter(true)
    adapter.on(Adapter.Event.RESUME, (e) => {
      expect(e.type).toBe(Adapter.Event.RESUME)
      done()
    })
    adapter.fireStart()
    adapter.fireJoin()
    adapter.firePause()
    adapter.fireResume()

    expect(adapter.flags.isPaused).toBe(false)
    expect(adapter.chronos.pause.stopTime).not.toBe(0)
  })

  it('should convert buffer from seek', (done) => {
    var adapter = new Adapter(true)
    adapter.plugin = {
      getIsLive: function () { return false }
    }
    adapter.on(Adapter.Event.BUFFER_BEGIN, (e) => {
      expect(e.type).toBe(Adapter.Event.BUFFER_BEGIN)
      done()
    })
    adapter.fireStart()
    adapter.fireJoin()
    adapter.fireSeekBegin()
    adapter.fireBufferBegin({}, true)

    expect(adapter.flags.isBuffering).toBe(true)
    expect(adapter.flags.isSeeking).toBe(false)
    expect(adapter.chronos.buffer.startTime).not.toBe(0)
    expect(adapter.chronos.seek.startTime).toBe(0)
  })

  it('should /buffer-begin', (done) => {
    var adapter = new Adapter(true)
    adapter.on(Adapter.Event.BUFFER_BEGIN, (e) => {
      expect(e.type).toBe(Adapter.Event.BUFFER_BEGIN)
      done()
    })
    adapter.fireStart()
    adapter.fireJoin()
    adapter.fireBufferBegin()

    expect(adapter.flags.isBuffering).toBe(true)
    expect(adapter.chronos.buffer.startTime).not.toBe(0)
  })

  it('should /buffer-end', (done) => {
    var adapter = new Adapter(true)
    adapter.on(Adapter.Event.BUFFER_END, (e) => {
      expect(e.type).toBe(Adapter.Event.BUFFER_END)
      done()
    })
    adapter.fireStart()
    adapter.fireJoin()
    adapter.fireBufferBegin()
    adapter.fireBufferEnd()

    expect(adapter.flags.isBuffering).toBe(false)
    expect(adapter.chronos.buffer.stopTime).not.toBe(0)
  })

  it('should not /buffer-end, but cancel buffer', () => {
    var adapter = new Adapter(true)
    adapter.fireStart()
    adapter.fireJoin()
    adapter.fireBufferBegin()
    adapter.cancelBuffer()

    expect(adapter.flags.isBuffering).toBe(false)
    expect(adapter.chronos.buffer.stopTime).not.toBe(0)
  })

  it('should convert seek from buffer', (done) => {
    var adapter = new Adapter(true)
    adapter.plugin = {
      getIsLive: function () { return false }
    }
    adapter.on(Adapter.Event.SEEK_BEGIN, (e) => {
      expect(e.type).toBe(Adapter.Event.SEEK_BEGIN)
      done()
    })
    adapter.fireStart()
    adapter.fireJoin()
    adapter.fireBufferBegin()
    adapter.fireSeekBegin({}, true)

    expect(adapter.flags.isBuffering).toBe(false)
    expect(adapter.flags.isSeeking).toBe(true)
    expect(adapter.chronos.buffer.startTime).toBe(0)
    expect(adapter.chronos.seek.startTime).not.toBe(0)
  })

  it('should /seek-begin', (done) => {
    var adapter = new Adapter(true)
    adapter.plugin = {
      getIsLive: function () { return false }
    }
    adapter.on(Adapter.Event.SEEK_BEGIN, (e) => {
      expect(e.type).toBe(Adapter.Event.SEEK_BEGIN)
      done()
    })
    adapter.fireStart()
    adapter.fireJoin()
    adapter.fireSeekBegin()

    expect(adapter.flags.isSeeking).toBe(true)
    expect(adapter.chronos.seek.startTime).not.toBe(0)
  })

  it('should NOT /seek', () => {
    var adapter = new Adapter(true)
    adapter.plugin = {
      getIsLive: function () { return true },
      options: {
        'content.isLive.noSeek': true
      }
    }

    adapter.fireStart()
    adapter.fireJoin()
    adapter.fireSeekBegin()

    expect(adapter.flags.isSeeking).toBe(false)

    adapter.fireSeekEnd()

    expect(adapter.flags.isSeeking).toBe(false)
  })

  it('should /seek-end', (done) => {
    var adapter = new Adapter(true)
    adapter.plugin = {
      getIsLive: function () { return false }
    }
    adapter.on(Adapter.Event.SEEK_END, (e) => {
      expect(e.type).toBe(Adapter.Event.SEEK_END)
      done()
    })
    adapter.fireStart()
    adapter.fireJoin()
    adapter.fireSeekBegin()
    adapter.fireSeekEnd()

    expect(adapter.flags.isSeeking).toBe(false)
    expect(adapter.chronos.seek.stopTime).not.toBe(0)
  })

  it('should not /seek-end, but cancel seek', () => {
    var adapter = new Adapter(true)
    adapter.plugin = {
      getIsLive: function () { return false }
    }
    adapter.fireStart()
    adapter.fireJoin()
    adapter.fireSeekBegin()
    adapter.cancelSeek()

    expect(adapter.flags.isSeeking).toBe(false)
    expect(adapter.chronos.seek.stopTime).not.toBe(0)
  })

  it('should /stop', (done) => {
    var adapter = new Adapter(true)
    adapter.on(Adapter.Event.STOP, (e) => {
      expect(e.type).toBe(Adapter.Event.STOP)
      done()
    })
    adapter.plugin = {
      _adapter: null,
      _isStopReady: function () {
        return true
      }
    }

    adapter.fireStart()
    adapter.fireStop()

    expect(adapter.flags.isStarted).toBe(false)
    expect(adapter.chronos.total.fireStopTime).not.toBe(0)
  })

  it('should /error', (done) => {
    var adapter = new Adapter(true)
    adapter.on(Adapter.Event.ERROR, (e) => {
      expect(e.type).toBe(Adapter.Event.ERROR)
      done()
    })

    adapter.fireError()
  })

  it('should /fatalError', (done) => {
    var adapter = new Adapter(true)
    adapter.on(Adapter.Event.ERROR, (e) => {
      expect(e.type).toBe(Adapter.Event.ERROR)
      done()
    })

    adapter.fireFatalError()
  })

  it('should /stop (casted)', (done) => {
    var adapter = new Adapter(true)

    adapter.plugin = {
      _isStopReady: function () {
        return true
      }
    }
    adapter.on(Adapter.Event.STOP, (e) => {
      expect(e.type).toBe(Adapter.Event.STOP)
      done()
    })

    adapter.fireStart()
    adapter.fireCasted()

    expect(adapter.flags.isStarted).toBe(false)
    expect(adapter.chronos.total.fireStopTime).not.toBe(0)
  })

  it('should /adClick', (done) => {
    var adapter = new Adapter(true)
    adapter.on(Adapter.Event.CLICK, (e) => {
      expect(e.type).toBe(Adapter.Event.CLICK)
      done()
    })

    adapter.fireStart()
    adapter.fireClick()
  })

  it('should /adClick with url', (done) => {
    var adapter = new Adapter(true)
    adapter.on(Adapter.Event.CLICK, (e) => {
      expect(e.type).toBe(Adapter.Event.CLICK)
      expect(e.data.params.url).toBe('fakeurl.com')
      done()
    })

    adapter.fireStart()
    adapter.fireClick('fakeurl.com')
  })

  it('should /adQuartile', (done) => {
    var adapter = new Adapter(true)
    adapter.on(Adapter.Event.QUARTILE, (e) => {
      expect(e.type).toBe(Adapter.Event.QUARTILE)
      done()
    })

    adapter.fireStart()
    adapter.fireQuartile(2)
  })

  it('should /adStop with skip', (done) => {
    var adapter = new Adapter(true)
    adapter.plugin = {
      _isStopReady: function () {
        return true
      }
    }
    adapter.on(Adapter.Event.STOP, (e) => {
      expect(e.type).toBe(Adapter.Event.STOP)
      done()
    })

    adapter.fireStart()
    adapter.fireSkip()
  })

  it('should /adBreakStart and /adBreakStop', (done) => {
    var adapter = new Adapter(true)
    adapter.plugin = {
      isBreakStarted: false,
      _isStopReady: function () { return true }
    }
    adapter.on(Adapter.Event.PODSTART, (e) => {
      expect(e.type).toBe(Adapter.Event.PODSTART)
    })
    adapter.on(Adapter.Event.PODSTOP, (e) => {
      expect(e.type).toBe(Adapter.Event.PODSTOP)
      done()
    })

    adapter.fireStart()
    adapter.fireBreakStart()
    adapter.fireStop()
    adapter.fireBreakStop()
  })

  it('should /adManifest manually with error code', (done) => {
    var adapter = new Adapter(true)
    adapter.on(Adapter.Event.MANIFEST, (e) => {
      expect(e.type).toBe(Adapter.Event.MANIFEST)
      expect(e.data.params.errorType).toBe('errorcode')
      expect(e.data.params.errorMessage).toBe('errormsg')
      done()
    })
    adapter.fireManifest('errorcode', 'errormsg')
  })

  it('should /adStop when calling fireSkip, no params', (done) => {
    var adapter = new Adapter(true)
    adapter.plugin = {
      _isStopReady: function () {
        return true
      }
    }
    adapter.on(Adapter.Event.STOP, (e) => {
      expect(e.type).toBe(Adapter.Event.STOP)
      expect(e.data.params.skipped).toBe(true)
      done()
    })
    adapter.fireStart()
    adapter.fireSkip()
  })

  it('should /adStop when calling fireSkip, additional params', (done) => {
    var adapter = new Adapter(true)
    adapter.plugin = {
      _isStopReady: function () {
        return true
      }
    }
    adapter.on(Adapter.Event.STOP, (e) => {
      expect(e.type).toBe(Adapter.Event.STOP)
      expect(e.data.params.key).toBe('value')
      done()
    })
    adapter.fireStart()
    adapter.fireSkip({ 'key': 'value' })
  })

  it('should /event with right params', (done) => {
    var adapter = new Adapter(true)
    var dimensions = {
      'dim1': 'a',
      'dim2': 'b'
    }
    var values = {
      'val1': 1,
      'val2': 2
    }
    var name = 'eventCustomName'
    adapter.on(Adapter.Event.VIDEO_EVENT, (e) => {
      expect(e.type).toBe(Adapter.Event.VIDEO_EVENT)
      expect(e.data.params.name).toBe(name)
      expect(e.data.params.dimensions).toBe(dimensions)
      expect(e.data.params.values).toBe(values)
      done()
    })
    adapter.fireEvent(name, dimensions, values)
  })
})

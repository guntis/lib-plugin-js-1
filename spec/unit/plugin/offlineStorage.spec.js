describe('offlineStorage', () => {
  var OfflineStorage = require('../../../src/plugin/offlineStorage')

  it('Should be created', () => {
    var os = new OfflineStorage()
    expect(os.actualView).toBe(null)
    expect(os.disabledStorage).toBeFalsy()
  })

  it('Should write start, creating new view id', () => {
    var os = new OfflineStorage()
    spyOn(os, '_newView')
    os.addEvent('/start', { 'param1': true, 'param2': false })
    expect(os._newView).toHaveBeenCalled()
  })

  it('Should write pause, not creating new view id', () => {
    var os = new OfflineStorage()
    spyOn(os, '_newView')
    os.addEvent('/pause', { 'param1': true, 'param2': false })
    expect(os._newView).not.toHaveBeenCalled()
  })

  it('Should not write init', () => {
    var os = new OfflineStorage()
    spyOn(os, '_newView')
    spyOn(localStorage, 'setItem')
    os.addEvent('/init', { 'param1': true, 'param2': false })
    expect(os._newView).not.toHaveBeenCalled()
    expect(localStorage.setItem).not.toHaveBeenCalled()
  })

  it('Should have stored views', () => {
    var os = new OfflineStorage()
    os.addEvent('/start', {
      'param1': true,
      'param2': false,
      'code': 'testcode',
      'sessionId': 'testId',
      'extraparam': 'stringvalue',
      'object': {
        'a': 'b'
      }
    })
    expect(os.getView()).not.toBeUndefined()
  })

  it('Should remove views', () => {
    var os = new OfflineStorage()
    os.addEvent('/start', {
      'param1': true,
      'param2': false,
      'code': 'testcode',
      'sessionId': 'testId',
      'extraparam': 'stringvalue',
      'object': {
        'a': 'b'
      }
    })
    os.removeView(os.actualView)
    expect(os.actualView).toBeNull()
  })

})

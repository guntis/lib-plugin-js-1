describe('Block detector', () => {
  var BlockDetector = require('../../../src/detectors/blockDetector')
  var plugin = {
    'options': {
      'app.https': false
    }
  }

  it('should not work, without navigator', () => {
    var detector = new BlockDetector(plugin)
    expect(detector.xhr).toBeDefined()
  })

  it('should save blocked value', () => {
    var detector = new BlockDetector(plugin)
    detector.blocked()
    expect(detector.isBlocked).toBeTruthy()
  })

  it('should save not blocked value', () => {
    var detector = new BlockDetector(plugin)
    detector.notBlocked()
    expect(detector.isBlocked).toBeFalsy()
  })
})

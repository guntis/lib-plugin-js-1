describe('HlsTransform', () => {
  var HlsTransform = require('../../../../../src/comm/transform/resourceparsers/hlsparser')
  var ht

  beforeEach(() => {
    ht = new HlsTransform()
  })

  it('should parse ts', () => {
    ht.parse('#EXT3\n' + 'file.ts')
    expect(ht.getResource()).toBe('file.ts')
  })
})

describe('Youbora Request', () => {
  var YBRequest = require('../../../src/comm/request')
  var request
  var cb = () => { }

  beforeEach(() => {
    global.spyXHR()
    request = new YBRequest('http://domain.com', '/service', { param: 'value' })
  })

  it('should send requests', () => {
    request.send()
    expect(global.XMLHttpRequest.prototype.open).toHaveBeenCalled()
    expect(global.XMLHttpRequest.prototype.send).toHaveBeenCalled()
  })

  it('should return request', () => {
    expect(request.getXHR()).toBeDefined()
  })

  it('should add listeners to requests', () => {
    request.on(YBRequest.Event.SUCCESS, cb)
    expect(global.XMLHttpRequest.prototype.addEventListener).toHaveBeenCalled()
  })

  it('should remove listeners', () => {
    request.off(YBRequest.Event.SUCCESS, cb)
    expect(global.XMLHttpRequest.prototype.removeEventListener).toHaveBeenCalled()
  })

  it('should add global events', () => {
    YBRequest.onEvery(YBRequest.Event.SUCCESS, cb)
    expect(YBRequest._globalListeners[YBRequest.Event.SUCCESS].length).toBe(1)
    YBRequest.offEvery(YBRequest.Event.SUCCESS, cb)
  })

  it('should remove global events', () => {
    YBRequest.onEvery(YBRequest.Event.SUCCESS, cb)
    YBRequest.offEvery(YBRequest.Event.SUCCESS, cb)

    expect(YBRequest._globalListeners[YBRequest.Event.SUCCESS].length).toBe(0)
  })

  it('should have params', () => {
    expect(request.getParam('param')).toBe('value')
    expect(request.getParam('timemark')).toBeDefined()
  })

  it('should add params', () => {
    request.setParam('param2', 'value2')
    expect(request.getParam('param2')).toBe('value2')
  })

  it('should stringify object params', () => {
    request.setParam('param', { a: 'b' })
    expect(request.getParamString()).toMatch(/param=%7B%22a%22%3A%22b%22%7D&timemark=(.+)/)
  })

  it('should modify params', () => {
    request.setParam('param', 'value2')
    expect(request.getParam('param')).toBe('value2')
  })

  it('should build paramstrings', () => {
    expect(request.getParamString()).toMatch(/param=value&timemark=(.+)/)
  })

  it('should build urls', () => {
    expect(request.getUrl()).toMatch(/http:\/\/domain.com\/service\?param=value&timemark=(.+)/)
  })
})

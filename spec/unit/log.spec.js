describe('Log', () => {
  var Log

  beforeEach(() => {
    Log = require('../../src/log')
    console.log = () => { }
    console.error = () => { }
    console.info = () => { }
    console.debug = () => { }
    console.warn = () => { }
    Log.logLevel = Log.Level.VERBOSE
  })

  it('should send events when noticing', (done) => {
    Log.on(Log.Event.LOG, () => { done() })
    Log.notice('notice')
  })

  it('should send events when debugging', (done) => {
    Log.on(Log.Event.LOG, () => { done() })
    Log.debug('notice')
  })

  it('should send events when verbosing', (done) => {
    Log.on(Log.Event.LOG, () => { done() })
    Log.verbose('notice')
  })

  it('should send events when warning', (done) => {
    Log.on(Log.Event.LOG, () => { done() })
    Log.warn('notice')
  })

  it('should send events when erroring', (done) => {
    Log.on(Log.Event.LOG, () => { done() })
    Log.error('notice')
  })

  it('should listen and unlisten', (done) => {
    var cbA = function () { done() }
    var cbB = function () { done.fail() }
    Log.on(Log.Event.LOG, cbA)
    Log.on(Log.Event.LOG, cbB)
    Log.off(Log.Event.LOG, cbB)
    Log.emit(Log.Event.LOG)
  })

  it('regression test', (done) => {
    // I can't test any of these, but at least I can assure that no error is thrown.
    Log.plainLogs = true
    Log.on(Log.Event.LOG, () => { done() })
    Log.notice('msg')
    Log.notice({})
    Log.plainLogs = false
    Log.loadLevelFromUrl()

    Log.report('msg')
  })
})
